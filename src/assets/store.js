import { createStore } from 'vuex'
import countries from './countries.json'
import categories from './categories.json'

const store = createStore({
    state () {
        return {
            isLoading: true,
            countries: [...countries],
            categories: [...categories],
            selectedCountry: "all",
            selectedCategory: "all",
            keywords: "",
            articles: [],
            page: 1,
            totalResults: 0,
            isTopNews: true,
            showMobileSearch: false,
            showArticleModal: null,
            articleModalData: null
        }
    },
    mutations: {
        setIsLoading (state, payload) {
            state.isLoading = payload.value
        },
        setCountry (state, payload) {
            state.selectedCountry = payload.value
        },
        setCategory (state, payload) {
            state.selectedCategory = payload.value
        },
        setKeywords (state, payload) {
            state.keywords = payload.value
        },
        setArticles (state, payload) {
            state.articles = payload.value
        },
        setPage (state, payload) {
            state.page = payload.value
        },
        setTotalResults (state, payload) {
            state.totalResults = payload.value
        },
        setIsTopNews (state, payload) {
            state.isTopNews = payload.value
        },
        setShowMobileSearch (state, payload) {
            state.showMobileSearch = payload.value
        },
        setShowArticleModal (state, payload) {
            state.showArticleModal = payload.value
        },
        setArticleModalData (state, payload) {
            state.articleModalData = payload.value
        }
    }
})

export default store