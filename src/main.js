import { createApp } from 'vue'
import App from './App.vue'
import store from './assets/store.js'
import './assets/main.css'

const app = createApp(App)

app.use(store)
app.mount('#app')

fetch(`https://newsapi.org/v2/top-headlines?pageSize=12&language=en&apiKey=d5479e7e0f0c4ed4a38e66a4034bdfe1`)
.then(response => response.json())
.then(response => {
    store.commit('setTotalResults', { value: response.totalResults })
    store.commit('setArticles', { value: response.articles ?? [] })
})
.catch(error => alert("ERROR: Failed to fetch news"))
.finally(() => store.commit('setIsLoading', { value: false }));